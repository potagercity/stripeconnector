<?php

/**
 * Stripe Payment Intents Gateway.
 */

namespace Omnipay\Stripe;

use Omnipay\Common\Message\AbstractRequest;
use Omnipay\Common\Message\RequestInterface;
use Omnipay\Stripe\Message\PaymentIntents\CancelPaymentIntentRequest;
use Omnipay\Stripe\Message\PaymentIntents\CaptureRequest;
use Omnipay\Stripe\Message\PaymentIntents\ConfirmPaymentIntentRequest;
use Omnipay\Stripe\Message\PaymentIntents\CreatePaymentMethodRequest;
use Omnipay\Stripe\Message\PaymentIntents\DetachPaymentMethodRequest;
use Omnipay\Stripe\Message\PaymentIntents\FetchPaymentIntentRequest;
use Omnipay\Stripe\Message\PaymentIntents\FetchPaymentMethodRequest;
use Omnipay\Stripe\Message\PaymentIntents\PurchaseRequest;
use Omnipay\Stripe\Message\PaymentIntents\UpdatePaymentMethodRequest;
use Omnipay\Stripe\Message\SetupIntents\CreateSetupIntentRequest;

/**
 * Stripe Payment Intents Gateway.
 *
 * @see AbstractGateway
 * @see \Omnipay\Stripe\Message\AbstractRequest
 *
 * @link https://stripe.com/docs/api
 *
 */
class PaymentIntentsGateway extends AbstractGateway
{
    /**
     * @inheritdoc
     */
    public function getName(): string
    {
        return 'Stripe Payment Intents';
    }

    /**
     * @inheritdoc
     *
     * @return AbstractRequest
     */
    public function authorize(array $parameters = array()) : RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\PaymentIntents\AuthorizeRequest', $parameters);
    }

    /**
     * @inheritdoc
     *
     * In reality, we're confirming the payment intent.
     * This method exists as an alias to in line with how Omnipay interfaces define things.
     *
     * @return ConfirmPaymentIntentRequest
     */
    public function completeAuthorize(array $options = []): RequestInterface
    {
        return $this->confirm($options);
    }

    /**
     * @inheritdoc
     *
     * @return CaptureRequest
     */
    public function capture(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\PaymentIntents\CaptureRequest', $parameters);
    }

    /**
     * Confirm a Payment Intent. Use this to confirm a payment intent created by a Purchase or Authorize request.
     *
     * @param array $parameters
     * @return ConfirmPaymentIntentRequest
     */
    public function confirm(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\PaymentIntents\ConfirmPaymentIntentRequest', $parameters);
    }

    /**
     * Cancel a Payment Intent.
     *
     * @param array $parameters
     * @return CancelPaymentIntentRequest
     */
    public function cancel(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\PaymentIntents\CancelPaymentIntentRequest', $parameters);
    }

    /**
     * @inheritdoc
     *
     * @return PurchaseRequest
     */
    public function purchase(array $options = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\PaymentIntents\PurchaseRequest', $options);
    }

    /**
     * @inheritdoc
     *
     * In reality, we're confirming the payment intent.
     * This method exists as an alias to in line with how Omnipay interfaces define things.
     *
     * @return ConfirmPaymentIntentRequest
     */
    public function completePurchase(array $options = [])
    {
        return $this->confirm($options);
    }

    /**
     * Fetch a payment intent. Use this to check the status of it.
     *
     * @return FetchPaymentIntentRequest
     */
    public function fetchPaymentIntent(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\PaymentIntents\FetchPaymentIntentRequest', $parameters);
    }

    //
    // Cards
    // @link https://stripe.com/docs/api/payment_methods
    //

    /**
     * @return FetchPaymentMethodRequest
     */
    public function fetchCard(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\PaymentIntents\FetchPaymentMethodRequest', $parameters);
    }

    /**
     * @inheritdoc
     *
     * @return CreatePaymentMethodRequest
     */
    public function createCard(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\PaymentIntents\CreatePaymentMethodRequest', $parameters);
    }

    /**
     * @inheritdoc
     *
     * @return CreatePaymentMethodRequest
     */
    public function attachCard(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\PaymentIntents\AttachPaymentMethodRequest', $parameters);
    }

    /**
     * @inheritdoc
     *
     * @return UpdatePaymentMethodRequest
     */
    public function updateCard(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\PaymentIntents\UpdatePaymentMethodRequest', $parameters);
    }

    /**
     * @inheritdoc
     *
     * @return DetachPaymentMethodRequest
     */
    public function deleteCard(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\PaymentIntents\DetachPaymentMethodRequest', $parameters);
    }

    // Setup Intent

    /**
     * @inheritdoc
     *
     * @return CreateSetupIntentRequest
     */
    public function createSetupIntent(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\SetupIntents\CreateSetupIntentRequest', $parameters);
    }

    /**
     * @inheritdoc
     *
     * @return CreateSetupIntentRequest
     */
    public function retrieveSetupIntent(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\SetupIntents\RetrieveSetupIntentRequest', $parameters);
    }
}
