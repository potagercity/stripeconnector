<?php

/**
 * Stripe Charge Gateway.
 */
namespace Omnipay\Stripe;

use Omnipay\Common\Message\AbstractRequest;
use Omnipay\Common\Message\RequestInterface;
use Omnipay\Stripe\Message\AttachSourceRequest;
use Omnipay\Stripe\Message\CancelSubscriptionRequest;
use Omnipay\Stripe\Message\CaptureRequest;
use Omnipay\Stripe\Message\CompletePurchaseRequest;
use Omnipay\Stripe\Message\CreateCardRequest;
use Omnipay\Stripe\Message\CreateInvoiceItemRequest;
use Omnipay\Stripe\Message\CreatePlanRequest;
use Omnipay\Stripe\Message\CreateSourceRequest;
use Omnipay\Stripe\Message\CreateSubscriptionRequest;
use Omnipay\Stripe\Message\CreateTokenRequest;
use Omnipay\Stripe\Message\DeleteCardRequest;
use Omnipay\Stripe\Message\DeleteInvoiceItemRequest;
use Omnipay\Stripe\Message\DeletePlanRequest;
use Omnipay\Stripe\Message\FetchEventRequest;
use Omnipay\Stripe\Message\FetchInvoiceItemRequest;
use Omnipay\Stripe\Message\FetchInvoiceLinesRequest;
use Omnipay\Stripe\Message\FetchInvoiceRequest;
use Omnipay\Stripe\Message\FetchPlanRequest;
use Omnipay\Stripe\Message\FetchSourceRequest;
use Omnipay\Stripe\Message\FetchSubscriptionRequest;
use Omnipay\Stripe\Message\FetchSubscriptionSchedulesRequest;
use Omnipay\Stripe\Message\FetchTokenRequest;
use Omnipay\Stripe\Message\FetchTransactionRequest;
use Omnipay\Stripe\Message\ListInvoicesRequest;
use Omnipay\Stripe\Message\ListPlansRequest;
use Omnipay\Stripe\Message\PurchaseRequest;
use Omnipay\Stripe\Message\UpdateCardRequest;
use Omnipay\Stripe\Message\UpdateSubscriptionRequest;

/**
 * Stripe Charge Gateway.
 *
 * @see AbstractGateway
 * @see Message\AbstractRequest
 *
 * @link https://stripe.com/docs/api
 *
 */
class Gateway extends AbstractGateway
{
    /**
     * @inheritdoc
     */
    public function getName(): string
    {
        return 'Stripe Charge';
    }

    /**
     * @inheritdoc
     *
     * @return AbstractRequest
     */
    public function authorize(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\AuthorizeRequest', $parameters);
    }

    /**
     * @inheritdoc
     *
     * @return CaptureRequest
     */
    public function capture(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\CaptureRequest', $parameters);
    }

    /**
     * @inheritdoc
     *
     * @return PurchaseRequest
     */
    public function purchase(array $options = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\PurchaseRequest', $options);
    }

    /**
     * @inheritdoc
     *
     * @return FetchTransactionRequest
     */
    public function fetchTransaction(array $options = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\FetchTransactionRequest', $options);
    }

    //
    // Cards
    // @link https://stripe.com/docs/api#cards
    //

    /**
     * @inheritdoc
     *
     * @return CreateCardRequest
     */
    public function createCard(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\CreateCardRequest', $parameters);
    }

    /**
     * @inheritdoc
     *
     * @return UpdateCardRequest
     */
    public function updateCard(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\UpdateCardRequest', $parameters);
    }

    /**
     * @inheritdoc
     *
     * @return DeleteCardRequest
     */
    public function deleteCard(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\DeleteCardRequest', $parameters);
    }

    //
    // Tokens
    // @link https://stripe.com/docs/api#tokens
    //

    /**
     * Creates a single use token that wraps the details of a credit card.
     * This token can be used in place of a credit card associative array with any API method.
     * These tokens can only be used once: by creating a new charge object, or attaching them to a customer.
     *
     * This kind of token is also useful when sharing clients between one platform and a connect account.
     * Use this request to create a new token to make a direct charge on a customer of the platform.
     *
     * @param array $parameters parameters to be passed in to the TokenRequest.
     * @return CreateTokenRequest The create token request.
     */
    public function createToken(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\CreateTokenRequest', $parameters);
    }

    /**
     * Stripe Fetch Token Request.
     *
     * Often you want to be able to charge credit cards or send payments
     * to bank accounts without having to hold sensitive card information
     * on your own servers. Stripe.js makes this easy in the browser, but
     * you can use the same technique in other environments with our token API.
     *
     * Tokens can be created with your publishable API key, which can safely
     * be embedded in downloadable applications like iPhone and Android apps.
     * You can then use a token anywhere in our API that a card or bank account
     * is accepted. Note that tokens are not meant to be stored or used more
     * than once—to store these details for use later, you should create
     * Customer or Recipient objects.
     *
     * @param array $parameters
     *
     * @return FetchTokenRequest
     */
    public function fetchToken(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\FetchTokenRequest', $parameters);
    }

    /**
     * Create Plan
     *
     * @param array $parameters
     * @return CreatePlanRequest
     */
    public function createPlan(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\CreatePlanRequest', $parameters);
    }

    /**
     * Fetch Plan
     *
     * @param array $parameters
     * @return FetchPlanRequest
     */
    public function fetchPlan(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\FetchPlanRequest', $parameters);
    }

    /**
     * Delete Plan
     *
     * @param array $parameters
     * @return DeletePlanRequest
     */
    public function deletePlan(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\DeletePlanRequest', $parameters);
    }

    /**
     * List Plans
     *
     * @param array $parameters
     * @return ListPlansRequest
     */
    public function listPlans(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\ListPlansRequest', $parameters);
    }

    /**
     * Create Subscription
     *
     * @param array $parameters
     * @return CreateSubscriptionRequest
     */
    public function createSubscription(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\CreateSubscriptionRequest', $parameters);
    }

    /**
     * Fetch Subscription
     *
     * @param array $parameters
     * @return FetchSubscriptionRequest
     */
    public function fetchSubscription(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\FetchSubscriptionRequest', $parameters);
    }

    /**
     * Update Subscription
     *
     * @param array $parameters
     * @return UpdateSubscriptionRequest
     */
    public function updateSubscription(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\UpdateSubscriptionRequest', $parameters);
    }

    /**
     * Cancel Subscription
     *
     * @param array $parameters
     * @return CancelSubscriptionRequest
     */
    public function cancelSubscription(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\CancelSubscriptionRequest', $parameters);
    }

    /**
     * Fetch Schedule Subscription
     *
     * @param array $parameters
     * @return FetchSubscriptionSchedulesRequest
     */
    public function fetchSubscriptionSchedules(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\FetchSubscriptionSchedulesRequest', $parameters);
    }

    /**
     * Fetch Event
     *
     * @param array $parameters
     * @return FetchEventRequest
     */
    public function fetchEvent(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\FetchEventRequest', $parameters);
    }

    /**
     * Fetch Invoice Lines
     *
     * @param array $parameters
     * @return FetchInvoiceLinesRequest
     */
    public function fetchInvoiceLines(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\FetchInvoiceLinesRequest', $parameters);
    }

    /**
     * Fetch Invoice
     *
     * @param array $parameters
     * @return FetchInvoiceRequest
     */
    public function fetchInvoice(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\FetchInvoiceRequest', $parameters);
    }

    /**
     * List Invoices
     *
     * @param array $parameters
     * @return ListInvoicesRequest
     */
    public function listInvoices(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\ListInvoicesRequest', $parameters);
    }

    /**
     * Create Invoice Item
     *
     * @param array $parameters
     * @return CreateInvoiceItemRequest
     */
    public function createInvoiceItem(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\CreateInvoiceItemRequest', $parameters);
    }

    /**
     * Fetch Invoice Item
     *
     * @param array $parameters
     * @return FetchInvoiceItemRequest
     */
    public function fetchInvoiceItem(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\FetchInvoiceItemRequest', $parameters);
    }

    /**
     * Delete Invoice Item
     *
     * @param array $parameters
     * @return DeleteInvoiceItemRequest
     */
    public function deleteInvoiceItem(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\DeleteInvoiceItemRequest', $parameters);
    }

    /**
     * @param array $parameters
     * @return CreateSourceRequest
     */
    public function createSource(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\CreateSourceRequest', $parameters);
    }

    /**
     * @param array $parameters
     * @return AttachSourceRequest
     */
    public function attachSource(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\AttachSourceRequest', $parameters);
    }

    /**
     * @param array $parameters
     * @return FetchSourceRequest
     */
    public function detachSource(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\DetachSourceRequest', $parameters);
    }

    /**
     * @param array $parameters
     * @return FetchSourceRequest
     */
    public function fetchSource(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\FetchSourceRequest', $parameters);
    }

    /**
     * Create a completePurchase request.
     *
     * @param array $parameters
     * @return CompletePurchaseRequest
     */
    public function completePurchase(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\CompletePurchaseRequest', $parameters);
    }

    /**
     * @param array $parameters
     *
     * @return AbstractRequest
     */
    public function createCoupon(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\CreateCouponRequest', $parameters);
    }

    /**
     * @param array $parameters
     *
     * @return AbstractRequest
     */
    public function fetchCoupon(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\FetchCouponRequest', $parameters);
    }

    /**
     * @param array $parameters
     *
     * @return AbstractRequest
     */
    public function deleteCoupon(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\DeleteCouponRequest', $parameters);
    }

    /**
     * @param array $parameters
     *
     * @return AbstractRequest
     */
    public function updateCoupon(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\UpdateCouponRequest', $parameters);
    }

    /**
     * @param array $parameters
     *
     * @return AbstractRequest
     */
    public function listCoupons(array $parameters = array()): RequestInterface
    {
        return $this->createRequest('\Omnipay\Stripe\Message\ListCouponsRequest', $parameters);
    }
}
